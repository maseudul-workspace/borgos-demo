package com.project.appdemo.activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.material.tabs.TabLayout;
import com.project.appdemo.R;
import com.project.appdemo.adapters.BrandsAdapter;
import com.project.appdemo.adapters.CompareCarAdapter;
import com.project.appdemo.adapters.ViewPagerNewCarAdapter;
import com.project.appdemo.adapters.ViewPagerNewsAdapter;
import com.project.appdemo.adapters.ViewpagerMainAdapter;
import com.project.appdemo.models.Brand;
import com.project.appdemo.models.CompareCar;
import com.project.appdemo.models.Image;
import com.project.appdemo.models.NewCar;
import com.project.appdemo.models.News;
import com.project.appdemo.models.NewsData;
import com.project.appdemo.util.GlideHelper;
import com.tbuonomo.viewpagerdotsindicator.DotsIndicator;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class MainActivity extends BaseActivity implements ViewPagerNewCarAdapter.Callback, LocationListener, GoogleApiClient.ConnectionCallbacks,
                                                                                            GoogleApiClient.OnConnectionFailedListener
{

    @BindView(R.id.main_viewpager)
    ViewPager mainViewPager;
    @BindView(R.id.dots_indicator)
    DotsIndicator dotsIndicator;
    @BindView(R.id.tab_layout_new_cars)
    TabLayout tabLayoutNewCars;
    @BindView(R.id.new_cars_view_pager)
    ViewPager viewPagerNewCars;
    @BindView(R.id.recycler_view_brands)
    RecyclerView recyclerViewBrands;
    @BindView(R.id.img_view_video_header)
    ImageView imgViewVideoHeader;
    @BindView(R.id.bottom_layout)
    View bottomLayout;
    @BindView(R.id.nested_scroll_view)
    NestedScrollView nestedScrollView;
    @BindView(R.id.recycler_view_compare_cars)
    RecyclerView recyclerViewCompareCars;
    @BindView(R.id.tab_layout_news)
    TabLayout tabLayoutNews;
    @BindView(R.id.news_view_pager)
    ViewPager viewPagerNews;
    Location mLastLocation;
    GoogleApiClient mGoogleApiClient;
    LocationRequest mLocationRequest;
    String[] appPremisions = {
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
    };
    private static final int PERMISSIONS_REQUEST_CODE = 1240;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        inflateContent(R.layout.activity_main);
        ButterKnife.bind(this);
        setMainViewPager();
        setViewPagerNewCars();
        setRecyclerViewBrands();
        setNestedScrollViewListener();
        setRecyclerViewCompareCars();
        setViewPagerNews();
        GlideHelper.setImageViewCustomRoundedCorners(this, imgViewVideoHeader, "https://gearopen.com/wp-content/uploads/2018/05/2018-Husqvarna-Vitpilen-401-9-695x538@2x.jpg", 100);
        if (checkAndRequestPermissions()) {
            buildGoogleApiClient();
        }
    }

    private void setMainViewPager() {
        ArrayList<Image> images = new ArrayList<>();

        Image image1 = new Image(1, "https://www.motorcyclespecs.co.za/Gallery_A-L_16/Harley-Bronx-975-01.jpg");
        Image image2 = new Image(2, "https://image.cnbcfm.com/api/v1/image/105130567-Vitpilen-701-street-bike--003.jpg?v=1529478010&w=678&h=381");
        Image image3 = new Image(3, "https://www.allstate.com/resources/Allstate/images/tools-and-resources/motorcycle/motorcycle-on-country-road_GettyImages_680x402.jpg");
        Image image4 = new Image(4, "https://kickstart.bikeexif.com/wp-content/uploads/2018/12/2019-triumph-speed-twin-specs-images-1-625x417.jpg");

        images.add(image1);
        images.add(image2);
        images.add(image3);
        images.add(image4);

        ViewpagerMainAdapter adapter = new ViewpagerMainAdapter(this, images);
        mainViewPager.setAdapter(adapter);
        dotsIndicator.setViewPager(mainViewPager);

    }

    private void setViewPagerNewCars() {
        ArrayList<NewCar> newCars = new ArrayList<>();
        NewCar newCar1 = new NewCar(1, "Cars under 5 lakh",
                                   "https://cars.tatamotors.com/images/tiago/features/performance/dynamic-performance-01.jpg", "Tata Tiago", "4.70 - 6.74 lakh", "4.1", "(123)",
                                    "https://images.news18.com/ibnlive/uploads/2019/02/New-Maruti-Suzuki-Wagon-R.jpg", "Maruti Wagon R", "4.69 - 6.14 lakh", "4.3", "(178)",
                                    "https://avtotachki.com/wp-content/uploads/2020/03/suzuki-celerio-2014-627x425.jpg", "Maruti Celerio", "3.49 - 5.64 lakh", "4.9", "(218)",
                                    "https://www.drivespark.com/img/2020/01/new-renault-kwid-launch-1580284593.jpg", "Renault Kwid", "3.19 - 5.54 lakh", "4.9", "(218)",
                                    "Popular Cars under 5 lakh"
                );

        NewCar newCar2 = new NewCar(2, "5 Lakh - 10 Lakh",
                "https://images.news18.com/ibnlive/uploads/2019/05/Hyundai-Venue-2.jpg", "Hyuandai Venue", "6.75 - 11.74 lakh", "4.1", "(123)",
                "https://images.carandbike.com/car-images/large/maruti-suzuki/baleno/maruti-suzuki-baleno.jpg?v=68", "Maruti Baleno", "5.70 - 9.14 lakh", "4.3", "(178)",
                "https://www.indiacarnews.com/wp-content/uploads/2016/05/Maruti-Swift-AMT.jpg", "Maruti Swift", "5.29 - 8.64 lakh", "4.9", "(218)",
                "https://akm-img-a-in.tosshub.com/sites/btmt/images/stories/hyundai-grand-i10-660_022119052533.jpg", "Hyundai Grand i10", "6.09 - 9.04 lakh", "4.9", "(218)",
                "Popular 5 Lakh - 10 Lakh Cars"
        );

        NewCar newCar3 = new NewCar(3, "10 Lakh - 15 Lakh",
                "https://stimg.cardekho.com/images/carexteriorimages/630x420/Hyundai/Elite-i20/7470/1592375864360/front-left-side-47.jpg?tr=h-140", "Hyuandai i20", "8.05 - 15.74 lakh", "4.1", "(123)",
                "https://images.carandbike.com/car-images/large/kia/seltos/kia-seltos.jpg?v=47", "Kia Seltos", "9.89 - 17.34 lakh", "4.3", "(178)",
                "https://cdni.autocarindia.com/Utils/ImageResizer.ashx?n=https%3A%2F%2Fcdni.autocarindia.com%2FExtraImages%2F20200327025041__DSC5174-copy.jpg&h=795&w=1200&c=1", "Hyundai Creta", "9.29 - 18.64 lakh", "4.9", "(218)",
                "https://stimg.cardekho.com/images/carexteriorimages/630x420/Tata/Altroz/7247/1578642800962/front-left-side-47.jpg?tr=h-140", "Tata Altroz", "8.19 - 15.24 lakh", "4.9", "(218)",
                "Popular 10 Lakh - 15 Lakh Cars"
        );

        newCars.add(newCar1);
        newCars.add(newCar2);
        newCars.add(newCar3);

        ViewPagerNewCarAdapter viewPagerNewCarAdapter = new ViewPagerNewCarAdapter(this, newCars, this);

        viewPagerNewCars.setAdapter(viewPagerNewCarAdapter);

        tabLayoutNewCars.setupWithViewPager(viewPagerNewCars);

    }

    private void setRecyclerViewBrands() {
        ArrayList<Brand> brands = new ArrayList<>();
        Brand brand1 = new Brand("https://www.car-logos.org/wp-content/uploads/2011/09/hyundai.png", "");
        Brand brand2 = new Brand("https://brandslogo.net/wp-content/uploads/2014/10/suzuki-logo-1.png", "");
        Brand brand3 = new Brand("https://i.pinimg.com/originals/13/ab/5f/13ab5fe6c536ff7125432ece35b8b217.png", "");
        Brand brand4 = new Brand("https://i.pinimg.com/originals/5b/ac/94/5bac942d02e70ce67498bf2ff04efe97.png", "");
        Brand brand5 = new Brand("https://i.pinimg.com/originals/17/38/ff/1738ff204f7eaaf912742070a0871f8e.jpg", "");
        Brand brand6 = new Brand("https://i.pinimg.com/originals/77/4e/e1/774ee176433f93b247e71a926a9dac15.png", "");
        Brand brand7 = new Brand("https://upload.wikimedia.org/wikipedia/commons/thumb/a/a0/Ford_Motor_Company_Logo.svg/640px-Ford_Motor_Company_Logo.svg.png", "");
        Brand brand8 = new Brand("https://upload.wikimedia.org/wikipedia/commons/thumb/6/67/Nissan-logo.svg/697px-Nissan-logo.svg.png", "");
        Brand brand9 = new Brand("https://www.carlogos.org/car-logos/datsun-logo.png", "");
        Brand brand10 = new Brand("https://seeklogo.com/images/M/MG-logo-C8D5AAF597-seeklogo.com.png", "");
        Brand brand11 = new Brand("https://assets.stickpng.com/thumbs/580b57fcd9996e24bc43c485.png", "");
        Brand brand12 = new Brand("https://i.pinimg.com/originals/37/ea/ed/37eaed19cf30f87b250503af0ee71b26.png", "");


        brands.add(brand1);
        brands.add(brand2);
        brands.add(brand3);
        brands.add(brand4);
        brands.add(brand5);
        brands.add(brand6);
        brands.add(brand7);
        brands.add(brand8);
        brands.add(brand9);
        brands.add(brand10);
        brands.add(brand11);
        brands.add(brand12);


        BrandsAdapter brandsAdapter = new BrandsAdapter(this, brands);
        recyclerViewBrands.setAdapter(brandsAdapter);
        recyclerViewBrands.setLayoutManager(new GridLayoutManager(this, 4));

    }

    private void setNestedScrollViewListener() {
        nestedScrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if (scrollY > oldScrollY) {
                    bottomLayout.animate().translationY(bottomLayout.getHeight()).setInterpolator(
                            new DecelerateInterpolator(2)
                    ).start();
                }
                if (scrollY < oldScrollY) {
                    bottomLayout.animate().translationY(0).setInterpolator(
                            new DecelerateInterpolator(2)
                    ).start();
                }
            }
        });
    }

    private void setRecyclerViewCompareCars() {
        ArrayList<CompareCar> compareCars = new ArrayList<>();

        CompareCar compareCar1 = new CompareCar(1, "https://cars.tatamotors.com/images/tiago/features/performance/dynamic-performance-01.jpg", "Tiago", "4.70 - 6.74 lakh",
                                                        "https://images.news18.com/ibnlive/uploads/2019/02/New-Maruti-Suzuki-Wagon-R.jpg", "Wagon R", "4.69 - 6.14 lakh");

        CompareCar compareCar2 = new CompareCar(1, "https://images.news18.com/ibnlive/uploads/2019/05/Hyundai-Venue-2.jpg", "Verna", "9.70 - 16.74 lakh",
                "https://images.carandbike.com/car-images/large/maruti-suzuki/baleno/maruti-suzuki-baleno.jpg?v=68", "Baleno", "9.69 - 12.14 lakh");

        CompareCar compareCar3 = new CompareCar(1, "https://images.carandbike.com/car-images/large/kia/seltos/kia-seltos.jpg?v=47", "Seltos", "9.70 - 12.74 lakh",
                "https://stimg.cardekho.com/images/carexteriorimages/630x420/Tata/Altroz/7247/1578642800962/front-left-side-47.jpg?tr=h-140", "Altroz", "5.69 - 13.14 lakh");

        compareCars.add(compareCar1);
        compareCars.add(compareCar2);
        compareCars.add(compareCar3);

        CompareCarAdapter compareCarAdapter = new CompareCarAdapter(this, compareCars);
        recyclerViewCompareCars.setAdapter(compareCarAdapter);
        recyclerViewCompareCars.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL,false));
    }

    private void setViewPagerNews() {
        ArrayList<NewsData> newsData = new ArrayList<>();

        ArrayList<News> newsArrayList1 = new ArrayList<>();

        News news1 = new News("https://mcmscache.epapr.in/post_images/website_300/post_17877395/thumb.jpg", "Toyota service network expanded to 87 new locations in India", "By Toyota", "Oct 27, 2020");
        News news2 = new News("https://cnet3.cbsistatic.com/img/iBiY3cYdTduXQVzMdLEcy_hz4QM=/756x425/2019/01/16/45538986-ca67-4635-8d8f-6ca2c4766a12/2020-porsche-911-carrera-s-001.jpg", "Rally inspired Porsche 911 inches towards reality", "By Porshe", "Oct 29, 2020");
        News news3 = new News("https://i.ytimg.com/vi/CA_LIZIdWws/maxresdefault.jpg", "2020 Hyundai i20 variants, engine and gearbox options revealed", "By Huandai", "Oct 27, 2020");
        News news4 = new News("https://cars.usnews.com/dims4/USNEWS/88cc31a/2147483647/resize/640x420%3E/format/jpeg/quality/85/?url=https%3A%2F%2Fcars.usnews.com%2Fstatic%2Fimages%2Farticle%2F201902%2F127967%2F1_genesis_g90_intro_slide_crop_ag9pnbuisng_640x420.jpg", "Genesis GV70 SUV revealed", "By Genesis", "Oct 27, 2020");

        newsArrayList1.add(news1);
        newsArrayList1.add(news2);
        newsArrayList1.add(news3);
        newsArrayList1.add(news4);

        ArrayList<News> newsArrayList2 = new ArrayList<>();

        News news5 = new News("https://imgd.aeplcdn.com/664x374/cw/ec/20361/Nissan-GTR-Exterior-84915.jpg?v=201711021421&q=85", "Nissan Magnite production begins ahead of price announcement", "By Nissan", "Oct 29, 2020");
        News news6 = new News("https://lh3.googleusercontent.com/proxy/Ap-C06I-Sp84zg-SBXYzMXL5ACE9CpouH0VI3a6YN07PYdgv40HD1oNZaVkD8n3-IgA_hgpCmXiNeqHtK1WQNgK_JMgjwe_FBYcAujVNZYD_Ti70hsH2ftjCZd7osuEpSlird7SICm1D5gtt8jBYSFpLjPvAHA", "Rally inspired Porsche 911 inches towards reality", "By Porshe", "Oct 29, 2020");
        News news7 = new News("https://www.driving.co.uk/s3/st-driving-prod/uploads/2017/01/1312999_11_EXT_PRESSIMAGES_UPDATE.jpg", "Kia plans to relaunch the brand with a new logo in 2021", "By Kia", "Oct 27, 2020");
        News news8 = new News("https://stimg.cardekho.com/images/carexteriorimages/930x620/MG/Hector/7481/1594701665685/front-left-side-47.jpg", "MG Hector Plus Style variants discontinued", "By Hector", "Oct 27, 2020");

        newsArrayList2.add(news5);
        newsArrayList2.add(news6);
        newsArrayList2.add(news7);
        newsArrayList2.add(news8);

        ArrayList<News> newsArrayList3 = new ArrayList<>();

        newsArrayList3.add(news1);
        newsArrayList3.add(news2);
        newsArrayList3.add(news3);
        newsArrayList3.add(news4);

        NewsData newsData1 = new NewsData("EXPERT REVIEWS", newsArrayList1);
        NewsData newsData2 = new NewsData("FEATURED STORIES", newsArrayList2);
        NewsData newsData3 = new NewsData("VIDEOS", newsArrayList3);

        newsData.add(newsData1);
        newsData.add(newsData2);
        newsData.add(newsData3);

        ViewPagerNewsAdapter adapter = new ViewPagerNewsAdapter(this, newsData);
        viewPagerNews.setAdapter(adapter);
        tabLayoutNews.setupWithViewPager(viewPagerNews);

    }

    @Override
    public void onCarImageClicked() {
        Intent intent = new Intent(this, VehicleDetailsActivity.class);
        startActivity(intent);
    }

    private boolean checkAndRequestPermissions() {
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String perm : appPremisions) {
            if (ContextCompat.checkSelfPermission(this, perm) != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(perm);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), PERMISSIONS_REQUEST_CODE);
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST_CODE) {
            HashMap<String, Integer> permissionResults = new HashMap<>();
            int deniedCount = 0;
            for (int i = 0; i < grantResults.length; i++) {
                if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                    permissionResults.put(permissions[i], grantResults[i]);
                    deniedCount++;
                }
            }

            if (deniedCount == 0) {
                buildGoogleApiClient();
            } else {
                for (Map.Entry<String, Integer> entry : permissionResults.entrySet()) {
                    String permName = entry.getKey();
                    int permResult = entry.getValue();
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, permName)) {
                        this.showAlertDialog("", "This app needs read and write storage permissions",
                                "Yes, Grant permissions",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        checkAndRequestPermissions();
                                    }
                                },
                                "Cancel",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        Toast.makeText(getApplicationContext(), "Permissions are required ", Toast.LENGTH_SHORT).show();
                                    }
                                },
                                false);
                    } else {
                        this.showAlertDialog("", "You have denied some permissions",
                                "Go to settings",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                                Uri.fromParts("package", getPackageName(), null));
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                    }
                                },
                                "Cancel",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        Toast.makeText(getApplicationContext(), "Permissions are required", Toast.LENGTH_SHORT).show();
                                    }
                                },
                                false);
                        break;
                    }

                }
            }
        }
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }

    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onLocationChanged(Location location) {


        mLastLocation = location;

        Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
        List<Address> addresses = null;
        try {
            addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            if (addresses.get(0).getLocality() != null) {
                getSupportActionBar().setTitle(addresses.get(0).getLocality());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        //stop location updates
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
    }

    public AlertDialog showAlertDialog(
            String title, String msg, String positiveLabel,
            DialogInterface.OnClickListener positiveOnClick,
            String negativeLabel, DialogInterface.OnClickListener negativeOnClick,
            boolean isCancelable
    ) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setCancelable(isCancelable);
        builder.setMessage(msg);
        builder.setPositiveButton(positiveLabel, positiveOnClick);
        builder.setNegativeButton(negativeLabel, negativeOnClick);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        return alertDialog;
    }
}