package com.project.appdemo.activities;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.content.Intent;
import android.os.Bundle;

import com.project.appdemo.R;

public class SignInActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        ButterKnife.bind(this);
    }

    private void goToMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.ttx_view_skip) void onSkipClicked() {
        goToMainActivity();
    }

    @OnClick(R.id.btn_fb_sign_in) void onFbSignInClicked() {
        goToMainActivity();
    }

    @OnClick(R.id.btn_google_sign_in) void onGoogleSignInClicked() {
        goToMainActivity();
    }

}