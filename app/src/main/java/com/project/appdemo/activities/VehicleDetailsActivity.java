package com.project.appdemo.activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.os.Bundle;
import android.text.Html;
import android.view.MenuItem;

import com.google.android.material.tabs.TabLayout;
import com.project.appdemo.R;
import com.project.appdemo.adapters.FragmentsAdapter;
import com.project.appdemo.fragments.FragmentVehicleDetailsGallery;
import com.project.appdemo.fragments.FragmentVehicleDetailsOverview;
import com.project.appdemo.fragments.FragmentVehicleDetailsPrice;
import com.project.appdemo.fragments.FragmentVehicleDetailsReviews;

public class VehicleDetailsActivity extends AppCompatActivity {

    @BindView(R.id.toolbar) @Nullable
    Toolbar toolbar;
    @BindView(R.id.tab_layout_vehicle_details)
    TabLayout tabLayoutVehicleDetails;
    @BindView(R.id.view_pager_vehicle_details)
    ViewPager viewPagerVehicleDetails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vehicle_details);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(Html.fromHtml("<b>BMW X1</b>"));
        setViewPagerVehicleDetails();
    }

    private void setViewPagerVehicleDetails() {

        FragmentVehicleDetailsOverview fragmentVehicleDetailsOverview = new FragmentVehicleDetailsOverview();
        FragmentVehicleDetailsPrice fragmentVehicleDetailsPrice = new FragmentVehicleDetailsPrice();
        FragmentVehicleDetailsGallery fragmentVehicleDetailsGallery = new FragmentVehicleDetailsGallery();
        FragmentVehicleDetailsReviews fragmentVehicleDetailsReviews = new FragmentVehicleDetailsReviews();

        FragmentsAdapter adapter = new FragmentsAdapter(getSupportFragmentManager());
        adapter.AddFragment(fragmentVehicleDetailsOverview, "OVERVIEW");
        adapter.AddFragment(fragmentVehicleDetailsPrice, "PRICE");
        adapter.AddFragment(fragmentVehicleDetailsGallery, "GALLERY");
        adapter.AddFragment(fragmentVehicleDetailsReviews, "REVIEWS");

        viewPagerVehicleDetails.setAdapter(adapter);
        tabLayoutVehicleDetails.setupWithViewPager(viewPagerVehicleDetails);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}