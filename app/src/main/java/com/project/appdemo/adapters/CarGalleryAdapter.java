package com.project.appdemo.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.project.appdemo.R;
import com.project.appdemo.models.Gallery;
import com.project.appdemo.models.Image;
import com.project.appdemo.util.GlideHelper;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class CarGalleryAdapter extends RecyclerView.Adapter<CarGalleryAdapter.ViewHolder> {

    Context mContext;
    ArrayList<Gallery> galleries;

    public CarGalleryAdapter(Context mContext, ArrayList<Gallery> galleries) {
        this.mContext = mContext;
        this.galleries = galleries;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_car_gallery, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.textViewCarSide.setText(galleries.get(position).carSide);
        GlideHelper.setImageView(mContext, holder.imgViewCar, galleries.get(position).image);
    }

    @Override
    public int getItemCount() {
        return galleries.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_car_side)
        TextView textViewCarSide;
        @BindView(R.id.img_view_car)
        ImageView imgViewCar;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
