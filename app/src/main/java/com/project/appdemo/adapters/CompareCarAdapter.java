package com.project.appdemo.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.project.appdemo.R;
import com.project.appdemo.models.CompareCar;
import com.project.appdemo.util.GlideHelper;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class CompareCarAdapter extends RecyclerView.Adapter<CompareCarAdapter.ViewHolder> {

    Context mContext;
    ArrayList<CompareCar> compareCars;

    public CompareCarAdapter(Context mContext, ArrayList<CompareCar> compareCars) {
        this.mContext = mContext;
        this.compareCars = compareCars;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_compare_cars, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewCarName1.setText(compareCars.get(position).carName1);
        holder.txtViewCarName2.setText(compareCars.get(position).carName2);

        holder.txtViewCarPrice1.setText(compareCars.get(position).carPrice1);
        holder.txtViewCarPrice2.setText(compareCars.get(position).carPrice2);

        GlideHelper.setImageView(mContext, holder.imgViewCar1, compareCars.get(position).carImage1);
        GlideHelper.setImageView(mContext, holder.imgViewCar2, compareCars.get(position).carImage2);

        holder.btnView.setText(compareCars.get(position).carName1 + " vs " + compareCars.get(position).carName2);

    }

    @Override
    public int getItemCount() {
        return compareCars.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_view_car_1)
        ImageView imgViewCar1;
        @BindView(R.id.img_view_car_2)
        ImageView imgViewCar2;
        @BindView(R.id.txt_view_car_name_1)
        TextView txtViewCarName1;
        @BindView(R.id.txt_view_car_name_2)
        TextView txtViewCarName2;
        @BindView(R.id.txt_view_car_price_1)
        TextView txtViewCarPrice1;
        @BindView(R.id.txt_view_car_price_2)
        TextView txtViewCarPrice2;
        @BindView(R.id.btn_view)
        Button btnView;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
