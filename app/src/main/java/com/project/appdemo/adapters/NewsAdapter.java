package com.project.appdemo.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.project.appdemo.R;
import com.project.appdemo.models.News;
import com.project.appdemo.util.GlideHelper;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.ViewHolder> {

    Context mContext;
    ArrayList<News> news;

    public NewsAdapter(Context mContext, ArrayList<News> news) {
        this.mContext = mContext;
        this.news = news;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_news, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewNewsTitle.setText(news.get(position).title);
        holder.txtViewNewsAuthor.setText(news.get(position).author);
        holder.txtViewNewsDate.setText(news.get(position).date);
        GlideHelper.setImageViewCustomRoundedCorners(mContext, holder.imgViewNews, news.get(position).image, 10);
    }

    @Override
    public int getItemCount() {
        return news.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_view_news)
        ImageView imgViewNews;
        @BindView(R.id.txt_view_news_title)
        TextView txtViewNewsTitle;
        @BindView(R.id.txt_view_news_author)
        TextView txtViewNewsAuthor;
        @BindView(R.id.txt_view_news_date)
        TextView txtViewNewsDate;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
