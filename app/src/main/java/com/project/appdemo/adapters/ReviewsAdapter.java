package com.project.appdemo.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.project.appdemo.R;
import com.project.appdemo.models.Reviews;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ReviewsAdapter extends RecyclerView.Adapter<ReviewsAdapter.ViewHolder> {

    Context mContext;
    ArrayList<Reviews> reviews;

    public ReviewsAdapter(Context mContext, ArrayList<Reviews> reviews) {
        this.mContext = mContext;
        this.reviews = reviews;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_reviews, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewReviewHeader.setText(reviews.get(position).reviewHeader);
        holder.txtViewReviewDesc.setText(reviews.get(position).reviewDesc);
        holder.txtViewReviewUser.setText(reviews.get(position).reviewUser);
        holder.txtViewReviewDate.setText(reviews.get(position).reviewDate);

    }

    @Override
    public int getItemCount() {
        return reviews.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_review_header)
        TextView txtViewReviewHeader;
        @BindView(R.id.txt_view_review_desc)
        TextView txtViewReviewDesc;
        @BindView(R.id.txt_view_review_user)
        TextView txtViewReviewUser;
        @BindView(R.id.txt_view_review_date)
        TextView txtViewReviewDate;
        @BindView(R.id.review_rating_bar)
        RatingBar ratingBar;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
