package com.project.appdemo.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.project.appdemo.R;
import com.project.appdemo.models.Variant;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class VariantsAdapter extends RecyclerView.Adapter<VariantsAdapter.ViewHolder> {

    Context mContext;
    ArrayList<Variant> variants;

    public VariantsAdapter(Context mContext, ArrayList<Variant> variants) {
        this.mContext = mContext;
        this.variants = variants;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_variants, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.textViewVariantName.setText(variants.get(position).variantName);
        holder.textViewVariantInfo.setText(variants.get(position).variantInfo);
        holder.textViewVariantPrice.setText(variants.get(position).variantPrice);
    }

    @Override
    public int getItemCount() {
        return variants.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_variant_name)
        TextView textViewVariantName;
        @BindView(R.id.txt_view_variant_info)
        TextView textViewVariantInfo;
        @BindView(R.id.txt_view_variant_price)
        TextView textViewVariantPrice;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
