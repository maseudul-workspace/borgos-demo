package com.project.appdemo.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.project.appdemo.R;
import com.project.appdemo.activities.VehicleDetailsActivity;
import com.project.appdemo.models.NewCar;
import com.project.appdemo.util.GlideHelper;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewpager.widget.PagerAdapter;

public class ViewPagerNewCarAdapter extends PagerAdapter {

    public interface Callback {
        void onCarImageClicked();
    }

    Context mContext;
    ArrayList<NewCar> newCars;
    Callback mCallback;

    public ViewPagerNewCarAdapter(Context mContext, ArrayList<NewCar> newCars, Callback mCallback) {
        this.mContext = mContext;
        this.newCars = newCars;
        this.mCallback = mCallback;
    }

    @Override
    public int getCount() {
        return newCars.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.layout_new_car_viewpager_layout, container, false);

        Button btnText = (Button) layout.findViewById(R.id.btn_view);

        TextView txtViewCarName1 = (TextView) layout.findViewById(R.id.txt_view_car_name_1);
        TextView txtViewCarPrice1 = (TextView) layout.findViewById(R.id.txt_view_car_price_1);
        TextView txtViewCarRating1 = (TextView) layout.findViewById(R.id.txt_view_rating_car_1);
        TextView txtViewCarRatingCount1 = (TextView) layout.findViewById(R.id.txt_view_rating_count_car_1);
        ImageView imgView1 = (ImageView) layout.findViewById(R.id.img_view_car_1);

        TextView txtViewCarName2 = (TextView) layout.findViewById(R.id.txt_view_car_name_2);
        TextView txtViewCarPrice2 = (TextView) layout.findViewById(R.id.txt_view_car_price_2);
        TextView txtViewCarRating2 = (TextView) layout.findViewById(R.id.txt_view_rating_car_2);
        TextView txtViewCarRatingCount2 = (TextView) layout.findViewById(R.id.txt_view_rating_count_car_2);
        ImageView imgView2 = (ImageView) layout.findViewById(R.id.img_view_car_2);

        TextView txtViewCarName3 = (TextView) layout.findViewById(R.id.txt_view_car_name_3);
        TextView txtViewCarPrice3 = (TextView) layout.findViewById(R.id.txt_view_car_price_3);
        TextView txtViewCarRating3 = (TextView) layout.findViewById(R.id.txt_view_rating_car_3);
        TextView txtViewCarRatingCount3 = (TextView) layout.findViewById(R.id.txt_view_rating_count_car_3);
        ImageView imgView3 = (ImageView) layout.findViewById(R.id.img_view_car_3);

        TextView txtViewCarName4 = (TextView) layout.findViewById(R.id.txt_view_car_name_4);
        TextView txtViewCarPrice4 = (TextView) layout.findViewById(R.id.txt_view_car_price_4);
        TextView txtViewCarRating4 = (TextView) layout.findViewById(R.id.txt_view_rating_car_4);
        TextView txtViewCarRatingCount4 = (TextView) layout.findViewById(R.id.txt_view_rating_count_car_4);
        ImageView imgView4 = (ImageView) layout.findViewById(R.id.img_view_car_4);

        txtViewCarName1.setText(newCars.get(position).carName1);
        txtViewCarPrice1.setText(newCars.get(position).carPrice1);
        txtViewCarRating1.setText(newCars.get(position).carRating1);
        txtViewCarRatingCount1.setText(newCars.get(position).carRatingCount1);
        GlideHelper.setImageViewCustomRoundedCorners(mContext, imgView1, newCars.get(position).carImage1, 20);

        txtViewCarName2.setText(newCars.get(position).carName2);
        txtViewCarPrice2.setText(newCars.get(position).carPrice2);
        txtViewCarRating2.setText(newCars.get(position).carRating2);
        txtViewCarRatingCount2.setText(newCars.get(position).carRatingCount2);
        GlideHelper.setImageViewCustomRoundedCorners(mContext, imgView2, newCars.get(position).carImage2, 20);

        txtViewCarName3.setText(newCars.get(position).carName3);
        txtViewCarPrice3.setText(newCars.get(position).carPrice3);
        txtViewCarRating3.setText(newCars.get(position).carRating3);
        txtViewCarRatingCount3.setText(newCars.get(position).carRatingCount3);
        GlideHelper.setImageViewCustomRoundedCorners(mContext, imgView3, newCars.get(position).carImage3, 20);

        txtViewCarName4.setText(newCars.get(position).carName4);
        txtViewCarPrice4.setText(newCars.get(position).carPrice4);
        txtViewCarRating4.setText(newCars.get(position).carRating4);
        txtViewCarRatingCount4.setText(newCars.get(position).carRatingCount4);
        GlideHelper.setImageViewCustomRoundedCorners(mContext, imgView4, newCars.get(position).carImage4, 20);

        btnText.setText(newCars.get(position).btnText);

        imgView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onCarImageClicked();
            }
        });

        imgView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onCarImageClicked();
            }
        });

        imgView3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onCarImageClicked();
            }
        });

        imgView4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onCarImageClicked();
            }
        });

        container.addView(layout);
        return layout;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return newCars.get(position).heading;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

}
