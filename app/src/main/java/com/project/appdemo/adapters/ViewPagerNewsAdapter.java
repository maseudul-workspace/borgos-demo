package com.project.appdemo.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.project.appdemo.R;
import com.project.appdemo.models.NewsData;
import com.project.appdemo.util.GlideHelper;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.PagerAdapter;

public class ViewPagerNewsAdapter extends PagerAdapter {

    Context mContext;
    ArrayList<NewsData> newsData;

    public ViewPagerNewsAdapter(Context mContext, ArrayList<NewsData> newsData) {
        this.mContext = mContext;
        this.newsData = newsData;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.layout_viewpager_news, container, false);
        RecyclerView recyclerViewNews = (RecyclerView) layout.findViewById(R.id.recycler_view_news);
        Button btnView = (Button) layout.findViewById(R.id.btn_view);
        NewsAdapter adapter = new NewsAdapter(mContext, newsData.get(position).news);
        recyclerViewNews.setAdapter(adapter);
        recyclerViewNews.setLayoutManager(new LinearLayoutManager(mContext));
        btnView.setText("Show All " + newsData.get(position).header);
        container.addView(layout);
        return layout;
    }

    @Override
    public int getCount() {
        return newsData.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return newsData.get(position).header;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

}
