package com.project.appdemo.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.project.appdemo.R;
import com.project.appdemo.models.Reviews;
import com.project.appdemo.models.ReviewsData;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.PagerAdapter;

public class ViewPagerReviewsAdapter extends PagerAdapter {

    Context mContext;
    ArrayList<ReviewsData> reviewsData;

    public ViewPagerReviewsAdapter(Context mContext, ArrayList<ReviewsData> reviewsData) {
        this.mContext = mContext;
        this.reviewsData = reviewsData;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.layout_view_pager_reviews, container, false);
        RecyclerView recyclerViewReviews = (RecyclerView) layout.findViewById(R.id.recycler_view_reviews);
        ReviewsAdapter adapter = new ReviewsAdapter(mContext, reviewsData.get(position).reviews);
        recyclerViewReviews.setAdapter(adapter);
        recyclerViewReviews.setLayoutManager(new LinearLayoutManager(mContext));
        recyclerViewReviews.addItemDecoration(new DividerItemDecoration(recyclerViewReviews.getContext(), DividerItemDecoration.VERTICAL));
        container.addView(layout);
        return layout;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return reviewsData.get(position).header;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return reviewsData.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }
}
