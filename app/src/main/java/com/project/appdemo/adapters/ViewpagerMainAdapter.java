package com.project.appdemo.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.project.appdemo.R;
import com.project.appdemo.models.Image;
import com.project.appdemo.util.GlideHelper;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewpager.widget.PagerAdapter;

public class ViewpagerMainAdapter extends PagerAdapter {

    Context mContext;
    ArrayList<Image> images;

    public ViewpagerMainAdapter(Context mContext, ArrayList<Image> images) {
        this.mContext = mContext;
        this.images = images;
    }

    @Override
    public int getCount() {
        return images.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.layout_main_viewpager, container, false);
        ImageView imageView = (ImageView) layout.findViewById(R.id.img_view_viewpager_main);
        GlideHelper.setImageViewCustomRoundedCorners(mContext, imageView, images.get(position).image, 30);
        container.addView(layout);
        return layout;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

}
