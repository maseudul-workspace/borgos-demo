package com.project.appdemo.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.project.appdemo.R;
import com.project.appdemo.models.VariantsData;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.PagerAdapter;

public class ViewpagerVariantsAdapter extends PagerAdapter {

    Context mContext;
    ArrayList<VariantsData> variantsData;

    public ViewpagerVariantsAdapter(Context mContext, ArrayList<VariantsData> variantsData) {
        this.mContext = mContext;
        this.variantsData = variantsData;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.layout_viewpager_variants, container, false);
        RecyclerView recyclerViewVariants = (RecyclerView) layout.findViewById(R.id.recycler_view_variants);
        VariantsAdapter adapter = new VariantsAdapter(mContext, variantsData.get(position).variants);
        recyclerViewVariants.setAdapter(adapter);
        recyclerViewVariants.setLayoutManager(new LinearLayoutManager(mContext));
        recyclerViewVariants.addItemDecoration(new DividerItemDecoration(recyclerViewVariants.getContext(), DividerItemDecoration.VERTICAL));
        container.addView(layout);
        return layout;
    }

    @Override
    public int getCount() {
        return variantsData.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return variantsData.get(position).header;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

}
