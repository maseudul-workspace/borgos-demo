package com.project.appdemo.fragments;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.project.appdemo.R;
import com.project.appdemo.adapters.CarGalleryAdapter;
import com.project.appdemo.models.Gallery;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FragmentVehicleDetailsGallery#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentVehicleDetailsGallery extends Fragment {

    Context mContext;
    @BindView(R.id.recycler_view_gallery)
    RecyclerView recyclerViewGallery;

    public FragmentVehicleDetailsGallery() {
        // Required empty public constructor
    }

    public static FragmentVehicleDetailsGallery newInstance(String param1, String param2) {
        FragmentVehicleDetailsGallery fragment = new FragmentVehicleDetailsGallery();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_vehicle_details_gallery, container, false);
        ButterKnife.bind(this, view);
        ArrayList<Gallery> galleries = new ArrayList<>();
        Gallery gallery1 = new Gallery("BMW X1 Front View", "https://www.bmw.co.uk/content/dam/bmw/marketGB/bmw_co_uk/bmw-cars/x-models/x1/2019/inspire/x1-inspire-hero-dt-1680x756.jpg");
        Gallery gallery2 = new Gallery("BMW X1 Top View", "https://stimg.cardekho.com/images/carexteriorimages/930x620/BMW/X1/7625/1583398555726/rear-left-view-121.jpg?tr=w-880,h-495");
        Gallery gallery3 = new Gallery("BMW X1 Front Fog Lamp", "https://stimg.cardekho.com/images/carexteriorimages/930x620/BMW/X1/7625/1583398555726/front-view-118.jpg?tr=w-880,h-495");
        Gallery gallery4 = new Gallery("BMW X1 Headlight", "https://stimg.cardekho.com/images/carexteriorimages/930x620/BMW/X1/7625/1583398555726/top-view-117.jpg?tr=w-880,h-495");
        Gallery gallery5 = new Gallery("BMW X1 Exterior Image", "https://stimg.cardekho.com/images/carexteriorimages/930x620/BMW/X1/7625/1583398555726/front-fog-lamp-41.jpg?tr=w-880,h-495");
        Gallery gallery6 = new Gallery("BMW X1 Exterior Image", "https://stimg.cardekho.com/images/carexteriorimages/930x620/BMW/X1/7625/1583398555726/headlight-43.jpg?tr=w-880,h-495");
        Gallery gallery7 = new Gallery("BMW X1 Rear Right Side", "https://stimg.cardekho.com/images/carexteriorimages/930x620/BMW/X1/7625/1583398555726/exterior-image-164.jpg?tr=w-880,h-495");
        Gallery gallery8 = new Gallery("BMW X1 Rear Left View", "https://stimg.cardekho.com/images/carexteriorimages/930x620/BMW/X1/7625/1583398555726/exterior-image-165.jpg?tr=w-880,h-495");

        galleries.add(gallery1);
        galleries.add(gallery2);
        galleries.add(gallery3);
        galleries.add(gallery4);
        galleries.add(gallery5);
        galleries.add(gallery6);
        galleries.add(gallery7);
        galleries.add(gallery8);

        CarGalleryAdapter adapter = new CarGalleryAdapter(mContext, galleries);
        recyclerViewGallery.setAdapter(adapter);
        recyclerViewGallery.setLayoutManager(new LinearLayoutManager(mContext));

        return view;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

}