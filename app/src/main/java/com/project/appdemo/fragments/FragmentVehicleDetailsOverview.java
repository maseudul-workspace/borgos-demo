package com.project.appdemo.fragments;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.google.android.material.tabs.TabLayout;
import com.project.appdemo.R;
import com.project.appdemo.adapters.VariantsAdapter;
import com.project.appdemo.adapters.ViewpagerVariantsAdapter;
import com.project.appdemo.models.Variant;
import com.project.appdemo.models.VariantsData;
import com.project.appdemo.util.GlideHelper;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FragmentVehicleDetailsOverview#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentVehicleDetailsOverview extends Fragment {

    @BindView(R.id.variants_view_pager)
    ViewPager variantsViewPager;
    @BindView(R.id.tab_layout_variants)
    TabLayout variantsTabLayout;
    @BindView(R.id.img_view_overview)
    ImageView imgViewOverview;
    @BindView(R.id.img_view_1)
    ImageView imgView1;
    @BindView(R.id.img_view_2)
    ImageView imgView2;
    @BindView(R.id.img_view_3)
    ImageView imgView3;
    Context mContext;

    public FragmentVehicleDetailsOverview() {
        // Required empty public constructor
    }

    public static FragmentVehicleDetailsOverview newInstance(String param1, String param2) {
        FragmentVehicleDetailsOverview fragment = new FragmentVehicleDetailsOverview();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_vehicle_details_overview, container, false);
        ButterKnife.bind(this, view);
        GlideHelper.setImageView(mContext, imgViewOverview, "https://www.bmw.co.uk/content/dam/bmw/marketGB/bmw_co_uk/bmw-cars/x-models/x1/2019/inspire/x1-inspire-hero-dt-1680x756.jpg");
        GlideHelper.setImageViewCustomRoundedCorners(mContext, imgView1, "https://www.bmw.in/content/bmw/marketIN/bmw_in/en_IN/all-models/x-series/X1/2019/inspire/jcr:content/par/highlight_2b64/highlightitems/highlightitem_4c61/image/mobile.transform/highlight/image.1560445021599.jpg", 16);
        GlideHelper.setImageViewCustomRoundedCorners(mContext, imgView2, "https://www.bmw.co.id/content/bmw/marketID/bmw_co_id/en_ID/all-models/x-series/X1/2019/inspire/jcr:content/par/highlight_2b64/highlightitems/highlightitem_8362/image/mobile.transform/highlight/image.1601988258247.jpg", 16);
        GlideHelper.setImageViewCustomRoundedCorners(mContext, imgView3, "https://c.ndtvimg.com/2020-02/8u6jh7g_2020-bmw-x1-facelift_625x300_08_February_20.jpg", 16);

        ArrayList<VariantsData> variantsData = new ArrayList<>();

        ArrayList<Variant> variants1 = new ArrayList<>();

        Variant variant1 = new Variant("sDrive20i SportX", "Rs. 35.90 Lakh", "1998 cc, Automatic, Petrol, 19.62 kmpl");
        Variant variant2 = new Variant("sDrive20i xLine", "Rs. 38.70 Lakh", "1998 cc, Automatic, Petrol, 19.62 kmpl");
        Variant variant3 = new Variant("sDrive20d xLine", "Rs. 39.90 Lakh", "1998 cc, Automatic, Petrol, 19.62 kmpl");
        Variant variant4 = new Variant("sDrive20d M Sport", "Rs. 42.90 Lakh", "1998 cc, Automatic, Petrol, 19.62 kmpl");

        variants1.add(variant1);
        variants1.add(variant2);
        variants1.add(variant3);
        variants1.add(variant4);

        ArrayList<Variant> variants2 = new ArrayList<>();

        variants2.add(variant1);
        variants2.add(variant2);

        ArrayList<Variant> variants3 = new ArrayList<>();

        variants3.add(variant3);
        variants3.add(variant4);

        VariantsData variantsData1 = new VariantsData("ALL VERSION", variants1);
        VariantsData variantsData2 = new VariantsData("DIESEL VERSION", variants2);
        VariantsData variantsData3 = new VariantsData("PETROL VERSION", variants3);

        variantsData.add(variantsData1);
        variantsData.add(variantsData2);
        variantsData.add(variantsData3);

        ViewpagerVariantsAdapter adapter = new ViewpagerVariantsAdapter(mContext, variantsData);
        variantsViewPager.setAdapter(adapter);
        variantsTabLayout.setupWithViewPager(variantsViewPager);

        return view;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

}