package com.project.appdemo.fragments;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.PopupMenu;
import androidx.fragment.app.Fragment;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.project.appdemo.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FragmentVehicleDetailsPrice#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentVehicleDetailsPrice extends Fragment {

    Context mContext;
    @BindView(R.id.txt_view_fuel_type)
    TextView textViewFuelType;
    @BindView(R.id.layout_fuel_type)
    View layoutFuelType;

    public FragmentVehicleDetailsPrice() {
        // Required empty public constructor
    }

    public static FragmentVehicleDetailsPrice newInstance(String param1, String param2) {
        FragmentVehicleDetailsPrice fragment = new FragmentVehicleDetailsPrice();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_vehicle_details_price, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @OnClick(R.id.layout_fuel_type) void onFuelTypeClicked() {
        PopupMenu popup = new PopupMenu(mContext, layoutFuelType);
        //inflating menu from xml resource
        popup.inflate(R.menu.popup_menu);
        //adding click listener
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.petrol_menu:
                        textViewFuelType.setText("Petrol ( 2 Variants )");
                        break;
                    case R.id.diesel_menu:
                        textViewFuelType.setText("Diesel ( 2 Variants )");
                        break;
                }
                return false;
            }
        });
        popup.show();

    }

}