package com.project.appdemo.fragments;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.google.android.material.tabs.TabLayout;
import com.project.appdemo.R;
import com.project.appdemo.adapters.ViewPagerReviewsAdapter;
import com.project.appdemo.models.Reviews;
import com.project.appdemo.models.ReviewsData;
import com.project.appdemo.util.GlideHelper;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FragmentVehicleDetailsReviews#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentVehicleDetailsReviews extends Fragment {

    Context mContext;
    @BindView(R.id.img_view_review_main)
    ImageView imgViewReviewMain;
    @BindView(R.id.tab_layout_reviews)
    TabLayout tabLayoutReviews;
    @BindView(R.id.reviews_view_pager)
    ViewPager reviewsViewPager;

    public FragmentVehicleDetailsReviews() {
        // Required empty public constructor
    }

    public static FragmentVehicleDetailsReviews newInstance(String param1, String param2) {
        FragmentVehicleDetailsReviews fragment = new FragmentVehicleDetailsReviews();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_vehicle_details_reviews, container, false);
        ButterKnife.bind(this, view);

        GlideHelper.setImageView(mContext, imgViewReviewMain, "https://www.bmw.co.uk/content/dam/bmw/marketGB/bmw_co_uk/bmw-cars/x-models/x1/2019/inspire/x1-inspire-hero-dt-1680x756.jpg");

        ArrayList<ReviewsData> reviewsDataArrayList = new ArrayList<>();

        ArrayList<Reviews> reviews1 = new ArrayList<>();

        Reviews review1 = new Reviews(5, "The Car Performs Really Nice.", "I am using BMW X1 Car and I like this car so much. This car performs very superbly. This car comes with a powerful engine and it offers many amazing features that provide...", "Dinesh Yadav", "Sep 24, 2020");
        Reviews review2 = new Reviews(5, "Amazing Car.", "BMW X1 Car looks very stylish and amazing. This car comes with a 1998cc powerful engine and an 8-speed dual-clutch automatic gearbox. This car runs very smoothly and give...", "Damini Singh", "Sep 24, 2020 ");
        Reviews review3 = new Reviews(5, "Designed To Impress", "BMW X1 is a luxury car. It is an amazing car. It is very comfortable. Its system is like mind-blowing because we stop the car the engine is automatically off when we open...", "Choudhary", "Sep 15, 2020");
        Reviews review4 = new Reviews(5, "Best Automatic Transmission.", "BMW car is the best and the other best part is the automatic transmission which is the future of the car, the BMW made one of the best automatic transmission.", "Amankumar Giri", "Oct 21, 2020");

        reviews1.add(review1);
        reviews1.add(review2);
        reviews1.add(review3);
        reviews1.add(review4);

        ArrayList<Reviews> reviews2 = new ArrayList<>();

        reviews2.add(review3);
        reviews2.add(review4);
        reviews2.add(review2);
        reviews2.add(review1);

        ReviewsData reviewsData1 = new ReviewsData("LATEST", reviews1);
        ReviewsData reviewsData2 = new ReviewsData("HELPFUL", reviews2);

        reviewsDataArrayList.add(reviewsData1);
        reviewsDataArrayList.add(reviewsData2);

        ViewPagerReviewsAdapter viewPagerReviewsAdapter = new ViewPagerReviewsAdapter(mContext, reviewsDataArrayList);

        reviewsViewPager.setAdapter(viewPagerReviewsAdapter);
        tabLayoutReviews.setupWithViewPager(reviewsViewPager);

        return view;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

}