package com.project.appdemo.models;

public class Brand {

    public String brandImage;
    public String brandName;

    public Brand(String brandImage, String brandName) {
        this.brandImage = brandImage;
        this.brandName = brandName;
    }
}
