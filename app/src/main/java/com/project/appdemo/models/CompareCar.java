package com.project.appdemo.models;

public class CompareCar {

    public int id;

    public String carImage1;
    public String carName1;
    public String carPrice1;

    public String carImage2;
    public String carName2;
    public String carPrice2;

    public CompareCar(int id, String carImage1, String carName1, String carPrice1, String carImage2, String carName2, String carPrice2) {
        this.id = id;
        this.carImage1 = carImage1;
        this.carName1 = carName1;
        this.carPrice1 = carPrice1;
        this.carImage2 = carImage2;
        this.carName2 = carName2;
        this.carPrice2 = carPrice2;
    }
}
