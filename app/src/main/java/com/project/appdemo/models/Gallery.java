package com.project.appdemo.models;

public class Gallery {

    public String carSide;
    public String image;

    public Gallery(String carSide, String image) {
        this.carSide = carSide;
        this.image = image;
    }
}
