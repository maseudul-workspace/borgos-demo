package com.project.appdemo.models;

public class Image {

    public int id;
    public String image;

    public Image(int id, String image) {
        this.id = id;
        this.image = image;
    }

}
