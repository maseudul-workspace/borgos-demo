package com.project.appdemo.models;

public class NewCar {

    public int id;
    public String heading;

    public String carImage1;
    public String carName1;
    public String carPrice1;
    public String carRating1;
    public String carRatingCount1;

    public String carImage2;
    public String carName2;
    public String carPrice2;
    public String carRating2;
    public String carRatingCount2;

    public String carImage3;
    public String carName3;
    public String carPrice3;
    public String carRating3;
    public String carRatingCount3;

    public String carImage4;
    public String carName4;
    public String carPrice4;
    public String carRating4;
    public String carRatingCount4;

    public String btnText;

    public NewCar(int id, String heading, String carImage1, String carName1, String carPrice1, String carRating1, String carRatingCount1, String carImage2, String carName2, String carPrice2, String carRating2, String carRatingCount2, String carImage3, String carName3, String carPrice3, String carRating3, String carRatingCount3, String carImage4, String carName4, String carPrice4, String carRating4, String carRatingCount4, String btnText) {
        this.id = id;
        this.heading = heading;
        this.carImage1 = carImage1;
        this.carName1 = carName1;
        this.carPrice1 = carPrice1;
        this.carRating1 = carRating1;
        this.carRatingCount1 = carRatingCount1;
        this.carImage2 = carImage2;
        this.carName2 = carName2;
        this.carPrice2 = carPrice2;
        this.carRating2 = carRating2;
        this.carRatingCount2 = carRatingCount2;
        this.carImage3 = carImage3;
        this.carName3 = carName3;
        this.carPrice3 = carPrice3;
        this.carRating3 = carRating3;
        this.carRatingCount3 = carRatingCount3;
        this.carImage4 = carImage4;
        this.carName4 = carName4;
        this.carPrice4 = carPrice4;
        this.carRating4 = carRating4;
        this.carRatingCount4 = carRatingCount4;
        this.btnText = btnText;
    }
}
