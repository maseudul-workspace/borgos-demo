package com.project.appdemo.models;

public class News {

    public String image;
    public String title;
    public String author;
    public String date;

    public News(String image, String title, String author, String date) {
        this.image = image;
        this.title = title;
        this.author = author;
        this.date = date;
    }
}
