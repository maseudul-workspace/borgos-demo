package com.project.appdemo.models;

import java.util.ArrayList;

public class NewsData {

    public String header;
    public ArrayList<News> news;

    public NewsData(String header, ArrayList<News> news) {
        this.header = header;
        this.news = news;
    }
}
