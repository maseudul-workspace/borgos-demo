package com.project.appdemo.models;

public class Reviews {

    public int rating;
    public String reviewHeader;
    public String reviewDesc;
    public String reviewUser;
    public String reviewDate;

    public Reviews(int rating, String reviewHeader, String reviewDesc, String reviewUser, String reviewDate) {
        this.rating = rating;
        this.reviewHeader = reviewHeader;
        this.reviewDesc = reviewDesc;
        this.reviewUser = reviewUser;
        this.reviewDate = reviewDate;
    }
}
