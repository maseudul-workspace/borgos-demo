package com.project.appdemo.models;

import java.util.ArrayList;

public class ReviewsData {

    public String header;
    public ArrayList<Reviews> reviews;

    public ReviewsData(String header, ArrayList<Reviews> reviews) {
        this.header = header;
        this.reviews = reviews;
    }
}
