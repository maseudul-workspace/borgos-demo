package com.project.appdemo.models;

public class Variant {

    public String variantName;
    public String variantPrice;
    public String variantInfo;

    public Variant(String variantName, String variantPrice, String variantInfo) {
        this.variantName = variantName;
        this.variantPrice = variantPrice;
        this.variantInfo = variantInfo;
    }
}
