package com.project.appdemo.models;

import java.util.ArrayList;

public class VariantsData {

    public String header;
    public ArrayList<Variant> variants;

    public VariantsData(String header, ArrayList<Variant> variants) {
        this.header = header;
        this.variants = variants;
    }
}
